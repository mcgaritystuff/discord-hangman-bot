const Mongoose = require('mongoose');
const Configuration = require('../config/configuration');

const UserPointReference = Mongoose.model('userPointReferences');

exports.getHangmanStats = async function(playerId) {
  let respectiveHangmanUser = await UserPointReference.findOne({ 
    "PlayerId": playerId,
    "GameName": 'hangman'
  });

  if (!respectiveHangmanUser) {
    respectiveHangmanUser = await new UserPointReference({
      "PlayerId": playerId,
      "GameName": 'hangman',
      "Points": 0
    }).save();
  }

  return respectiveHangmanUser;
};

exports.getTopThreeForGame = async function(message, client, gameName) {
  const topThree = await UserPointReference.find({"GameName": gameName}).sort({"Points":-1}).limit(3);
  
  if (topThree.length === 0) {
    message.reply("Either there are no users playing that or the game doesn't exist!");
    return;
  }

  let valueMessage = "";
  
  await topThree.map( async pointEntry => {
    const username = (await client.fetchUser(pointEntry.PlayerId)).username;
    valueMessage += `${pointEntry.Points} -> ${username}` + "\n";
  });
  
  message.reply({embed:{
    color: 3329330,
    author: {
      name: client.user.username,
      icon_url: client.user.avatarURL
    },
    title: `Top 3 Players`,
    fields: [{ 
        name: "Win count -> Player Name",
        value: `${valueMessage}`
      }]
  }});
};