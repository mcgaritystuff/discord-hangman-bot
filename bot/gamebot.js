const Discord = require('discord.js');
const Mongoose = require('mongoose');
const Configuration = require('../config/configuration');
const PlayerPoints = require('./player_points');
const GameManager = require('../games/game_manager');

const Client = new Discord.Client();
const UserPointReference = Mongoose.model('userPointReferences');
const Game = Mongoose.model('games');

class GameBot {  
  constructor(clientSecret) {
    this.clientSecret = clientSecret;
  }

  login() {
    Client.login(this.clientSecret);
    return Client;
  }

  async ready() {
    await GameManager.restartGames();
    console.log(`logged in as ${Client.user.username}!`);
  }
  
  async handleMessage(message) {
    const messageContents = message.content.split(" ");
    
    if (messageContents[0][0] !== ',' || message.author.id === Client.user.id) {
      return;
    }
    
    // When asking for a specific game command, we can't use the switch statement
    if (messageContents[0].startsWith(",top3-")) {
      const gameName = messageContents[0].substring(',top3-'.length, messageContents[0].length);
      PlayerPoints.getTopThreeForGame(message, Client, gameName);
      return;
    } else if (messageContents[0].startsWith(",join-")) {
      const gameName = messageContents[0].substring(',join-'.length, messageContents[0].length);
      GameManager.joinGame(gameName, message);
      return;
    } else if (messageContents[0].startsWith(",start-")) {
      const gameName = messageContents[0].substring(',start-'.length, messageContents[0].length);
      GameManager.startGame(gameName, message, Client);
      return;
    }
    
    switch (messageContents[0]) {
      case ',game':
        GameManager.handleGameCommand(message, 
        message.content.substring(',game'.length, message.content.length).trim());
        break;
      case ',help':
        this.showHelp(message);
        break;
      case ',inprogress':
        this.showInProgress(message);
        break;
      case ',marco':
        message.reply('polo');
        break;
      case ',mypoints':
        this.showMyPoints(message);
        break;
      default:
        message.reply("I don't know that command. Please type `,help` for options.");
        break;
    }
  }
  
  showHelp(message) {
    const gimardEmoji = Client.emojis.get('322146746996228096');
    const veraEmoji = Client.emojis.get('322146746677329921');
    let gamesAvailableString = "";
    
    Object.keys(GameManager.gamesAvailable).forEach( game => {
      const gameObject = GameManager.gamesAvailable[game];
      let emoji = Client.emojis.get(gameObject.emojiId);
      
      gamesAvailableString += `${emoji} ${gameObject.name} - ${gameObject.description}\n`;
    });
    
    message.reply({embed:{
      color: 3329330,
      title: `${gimardEmoji} GameBot Rice Patty Menu ${veraEmoji}`,
      fields: [{
          name: "Current Games Available:",
          value: gamesAvailableString
        }, {
          name: "Do an action on the current game:",
          value: "`,game [action]` => Do some kind of action for the game."
        }, {
          name: "Current Game:",
          value: "`,inprogress` => Display the status of the current games being played, if any."
        }, {
          name: "Marco:",
          value: "`,marco` => Responds with polo."
        }, {
          name: "Show my points:",
          value: "`,mypoints` => Prints out how many points you've accumulated."
        }, {
          name: "Join a given game:",
          value: "`,join-[game]` => Join a game that someone started. Example usage: `,join-hangman`"
        }, {
          name: "Start a Hangman game:",
          value: "`,start-[game]` => Start a new game (if one isn't already taking place). Example usage: `,start-hangman`"
        }, {
          name: "Show top players:",
          value: "`,top3-[game]` => Shows the top three high scores and the players for the respective game. Example usage: `,top3-hangman`."
        }]
    }});
  }
  
  async showMyPoints(message) {
    const hangmanStats = await PlayerPoints.getHangmanStats(message.author.id);
    
    message.reply({embed:{
      color: 3329330,
      author: {
        name: Client.user.username,
        icon_url: Client.user.avatarURL
      },
      title: `Game Stats`,
      fields: [{
          name: "Hangman:",
          value: `You currently have ${hangmanStats.Points} points`
        }]
    }});
  }
  
  async showInProgress(message) {
    let inProgressGameList = "", waitingGameList = "";
    const gamesInProgress = await Game.find({
      "InProgress": true
    });
    const gamesWaitingForPlayers = await Game.find({
      "WaitingForPlayers": true
    });
    
    await gamesInProgress.forEach( game => {
      const gameObject = GameManager.gamesAvailable[game.GameName];
      let emoji = Client.emojis.get(gameObject.emojiId);
      
      inProgressGameList += `${emoji} ${gameObject.name}\n`;
    });
    
    await gamesWaitingForPlayers.forEach( game => {
      const gameObject = GameManager.gamesAvailable[game.GameName];
      let emoji = Client.emojis.get(gameObject.emojiId);
      
      waitingGameList += `${emoji} ${gameObject.name}\n`;
    });
    
    if (inProgressGameList === "") {
      inProgressGameList = "None!";
    }
    if (waitingGameList === "") {
      waitingGameList = "None!";
    }
    
    message.reply({embed:{
      color: 3329330,
      author: {
        name: Client.user.username,
        icon_url: Client.user.avatarURL
      },
      title: `Games in progress`,
      fields: [{
          name: "These are the games in progress:",
          value: inProgressGameList
        },{
          name: "These are the games waiting for players to join:",
          value: waitingGameList
        }]
    }});
  }
}

exports.GameBot = GameBot;
exports.Client = Client;
