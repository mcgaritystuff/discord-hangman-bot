Versioning: z.y.x
z -> Major version update, requires downtime
y -> Requires database modifications and possible downtime
x -> New files, fixes, changes, etc. Nothing too noticeable

# v1.0.0
 * Initial creation of Hangman!
