const Mongoose = require('mongoose');

const Game = Mongoose.model('games');
const Hangman = require('../games/hangman');
const Configuration = require('../config/configuration');

const UserPointReference = Mongoose.model('userPointReferences');

let inprogressGame = "";
const gamesAvailable = {
  "hangman": { 
    "game": Hangman,
    "name": "Hangman",
    "description": 'Guess the word(s) and get points!',
    "emojiId": '322146746928988171'
  }
};

exports.gamesAvailable = gamesAvailable;

exports.joinGame = async function(gameName, message) {
  if (!(gameName in gamesAvailable)) {
    message.reply("I don't recognize that game, sorry :(");
    return;
  }
  
  const gameProgressInfo = await Game.findOne({ "GameName": gameName });
  
  if (!gameProgressInfo) {
   await new Game({
      "GameName": gameName,
      "InProgress": false,
      "WaitingForPlayers": false
    }).save(); 
  }
  
  if (gameProgressInfo.InProgress) {
    message.reply(`There's already a game of ${gameName} being played! Please wait until it's complete.`);
  } else if (!gameProgressInfo.WaitingForPlayers) {
    message.reply(`Someone needs to start a game of ${gameName} first!`);
  } else {
    gamesAvailable[gameName].game.joinGame(message);
    checkPlayerIsRegistered(message, gameName);
  }
};

async function checkPlayerIsRegistered(message, gameName) {
  let respectiveUser = await UserPointReference.findOne({ 
    "PlayerId": message.author.id,
    "GameName": gameName
  });

  if (!respectiveUser) {
    await new UserPointReference({
      "PlayerId": message.author.id,
      "GameName": gameName,
      "Points": 0
    }).save();
  }
}

exports.startGame = async function(gameName, message, client) {
  if (!(gameName in gamesAvailable)) {
    message.reply("I don't recognize that game, sorry :(");
    return;
  }
  
  const requestedGameInfo = await Game.findOne({ "GameName": gameName });
  const existingRunningGame = await Game.findOne({ 
    $or: [
     { "InProgress": true, },
     { "WaitingForPlayers": true }
    ]
  });
  
  // If the game don't exist, create it
  // else if it's in progress, tell the player to wait
  // else if it's waiting for players, tell the player to join
  if (!requestedGameInfo) {
    await new Game({
      "GameName": gameName,
      "InProgress": false,
      "WaitingForPlayers": false
    }).save();
  }
  
  // Make sure there aren't games being played or in pending states
  if (existingRunningGame) {
    if (existingRunningGame.InProgress) {
      message.reply(`There's already a game of ${existingRunningGame.GameName} being played! Please wait until it's complete.`);
    } else if (existingRunningGame.WaitingForPlayers) {
      message.reply(`There's already a game of ${existingRunningGame.GameName} waiting for players! Type \`,join-${existingRunningGame.GameName}\` to join!`);
    }
    return;
  }
  
  inprogressGame = gameName;
  gamesAvailable[gameName].game.startGame(message, client);

  await Game.updateOne(
    { "GameName": gameName },
    { $set: { "WaitingForPlayers": true } }
  );

  checkPlayerIsRegistered(message, gameName);
  message.reply(`The game for ${gameName} will start soon! Players who want to join, type \`,join-${gameName}\` within the next ${Configuration.GameStartTime/1000} seconds!`)
};

exports.restartGames = async function() {
  await Game.updateMany(
    {},
    { $set: { "WaitingForPlayers": false, "InProgress": false } }
  );
  console.log("Games all restarted!");
};

exports.handleGameCommand = async function(message, gameMessage) {
  // Validate the game's still actually being played
  const requestedGameInfo = await Game.findOne({ 
    $or: [
     { "InProgress": true, },
     { "WaitingForPlayers": true }
    ]
  });
  if (!requestedGameInfo) {
    inprogressGame = "";
  }
  
  if (inprogressGame && requestedGameInfo.InProgress) {
    gamesAvailable[inprogressGame].game.play(message, gameMessage);
  } else {
    message.reply("There aren't any games being played right now!");
  }
};