const Axios = require('axios');
const Mongoose = require('mongoose');
const Configuration = require('../config/configuration');
const Keys = require('../config/keys');

const UserPointReference = Mongoose.model('userPointReferences');
const Game = Mongoose.model('games');

let client = null; // The discord client
let currentPlayers = []; // Who is playing
let currentWord = ""; // What to show the players
let currentAnswer = ""; // The actual correct answer the player's need to find
let incorrectAttempts = 0; // The number of attempts that have been incorrect
let maxAttempts = 6; // The maximum attempts before the game ends
let badGuesses = []; // Which letters/words players have guessed wrong
let goodGuesses = []; // Which letters/words players have guessed correctly
let currentPlayerIndex = 0; // The player that's currently guessing the letter/word(s)
let skippedIndices = []; // Indices for players that will be skipped since they guessed the wrong words
let webUrl = ""; // The URL to the page pulled randomly
// Timeouts
let playerTurnTimeout = null;
let globalTimeout = null;

exports.joinGame = async function(message) {
  if (currentPlayers.includes(message.author.id)) {
    message.reply("You're already in the queue to play!");
    return;
  }
  
  currentPlayers.push(message.author.id);
  message.reply(`You're in the game! There are currently ${currentPlayers.length} competing players!`);
};

exports.startGame = async function(message, userClient) {
  // Reset values
  currentPlayers = [];
  currentAnswer = "";
  currentWord = "";
  badGuesses = [];
  goodGuesses = [];
  skippedIndices = [];
  currentPlayerIndex = 0;
  incorrectAttempts = 0;
  
  currentPlayers.push(message.author.id);
  client = userClient;
  
  let botChannel = message.guild.channels.find('id', Configuration.BotPlaygroundChannelId);
  setTimeout(async () => {
    await setNewHangmanWord();
    
    if (botChannel) {
      // If there was an error, just cancel the game and print out the error
      if (currentAnswer.startsWith("ERROR")) {
        botChannel.send(`THERE WAS AN ERROR RETRIEVING A WORD: ${currentAnswer}`);
        await Game.updateOne(
          { "GameName": 'hangman' },
          { $set: { "WaitingForPlayers": false, "InProgress": false } }
        );
      } else {
        await Game.updateOne(
          { "GameName": 'hangman' },
          { $set: { "WaitingForPlayers": false, "InProgress": true } }
        );
        
        let n = currentPlayers.length;
        while (n > 1) {
          n--;
          let k = Math.floor(Math.random() * n + 1);
          let value = currentPlayers[k];
          currentPlayers[k] = currentPlayers[n];
          currentPlayers[n] = value;
        }
        
        // Set to last player in array so that the first one in the array goes next
        currentPlayerIndex = currentPlayers.length - 1;
        nextPlayer(message);
        
        // Set the global timeout in case the game takes too long
        globalTimeout = setTimeout(async function() {
          await gameOver(message);
        }, Configuration.GlobalNoResponseTimeout);
      }
    }
  }, Configuration.GameStartTime);
  
  botChannel.send({embed:{
    color: 3329330,
    title: `The game will begin soon! Here are the instructions`,
    fields: [{
        name: "Guess a letter:",
        value: '`,game [letter]` => Guess a game letter'
      }, {
        name: "Guess the word(s):",
        value: '`,game [word(s)]` => Think you know the word(s)? Type them all! If you get it wrong, you lose a turn.'
      }]
  }});
};

exports.play = async function(message, gameMessage) {
  // Make sure the person playing is supposed to be. Ignore them if not
  if (message.author.id !== currentPlayers[currentPlayerIndex]) {
    return;
  }
  
  // Make sure there was an attempt at guessing
  if (!gameMessage) {
    message.reply("You need something to guess!");
    return;
  }
  
  // Lowercase it to make this a lil' bit cleaner
  gameMessage = gameMessage.toLowerCase();
  
  if (badGuesses.includes(gameMessage) || 
      goodGuesses.includes(gameMessage)) {
    message.reply('That was already guessed! Try again');
    return;
  }
  
  // If the content is longer than 1 in length, they're guessing a word!
  if (gameMessage.length > 1) {
    if (gameMessage === currentAnswer.toLowerCase()) {
      currentWord = currentAnswer;
    } else {
      message.reply(`WRONG!! You lose 1 turn!`);
      skippedIndices.push(currentPlayerIndex);
      incorrectAttempts += 1;
      badGuesses.push(gameMessage);
    }
  } else {
    // Get all the indices of the guessed character
    let indicesWithLetter = [];
    for(var i = 0; i < currentAnswer.length; i++) {
      if (currentAnswer[i].toLowerCase() === gameMessage) {
        indicesWithLetter.push(i);
      }
    }
    if (indicesWithLetter.length > 0 ) {
      indicesWithLetter.forEach( index => {
        currentWord = currentWord.substr(0,index) + 
                      currentAnswer[index] + 
                      currentWord.substr(index+1);
      });
      goodGuesses.push(gameMessage);
    } else {
      message.reply(`WRONG!`);
      incorrectAttempts += 1;
      badGuesses.push(gameMessage);
    }
  }
  
  if (currentWord === currentAnswer) {
    await winGame(message);
  } else if (incorrectAttempts === maxAttempts) {
    await gameOver(message);
  } else {
    await nextPlayer(message);
  }
};

async function gameOver(message) {
  await Game.updateOne(
    { "GameName": 'hangman' },
    { $set: { "InProgress": false } }
  );

  const botChannel = message.guild.channels.find('id', Configuration.BotPlaygroundChannelId);
  botChannel.send(`Gameover!\n${showHangman()}`);
  clearTimeout(playerTurnTimeout);
  clearTimeout(globalTimeout);
}

async function winGame(message) {
  await UserPointReference.updateOne(
    { "PlayerId": message.author.id, "GameName": 'hangman' },
    { $inc: { "Points": currentAnswer.length } }
  );
  await Game.updateOne(
    { "GameName": 'hangman' },
    { $set: { "InProgress": false } }
  );
  
  message.reply(`Holy moly, you got it!! You just got ${currentAnswer.length} points!\n${showHangman()}`);
  clearTimeout(playerTurnTimeout);
  clearTimeout(globalTimeout);
}

async function nextPlayer(message) {
  let skippedPlayers = "";
  let channelMessage = "";
  
  do {
    currentPlayerIndex = (currentPlayerIndex + 1) % currentPlayers.length;
    if (currentPlayerIndex in skippedIndices) {
      let skippedPlayer = await client.fetchUser(currentPlayers[currentPlayerIndex]);
      skippedPlayers += `${skippedPlayer} `;
      skippedIndices.splice(skippedIndices.indexOf(currentPlayerIndex), 1);
      // Skip!
      currentPlayerIndex = (currentPlayerIndex + 1) % currentPlayers.length;
    }
  } while (currentPlayerIndex in skippedIndices);
  
  if (skippedPlayers.length > 0) {
    channelMessage = `Skipped Players ${skippedPlayers};`;
  }
  
  const user = await client.fetchUser(currentPlayers[currentPlayerIndex]);
  channelMessage += `It's your turn ${user}! You've got ${Configuration.MaxTurnLength / 1000} seconds. Total game time is: ${Configuration.GlobalNoResponseTimeout / 1000 / 60} minutes.\n${showHangman()}`;
  clearTimeout(playerTurnTimeout);
  playerTurnTimeout = setTimeout(function() {
    nextPlayer(message);
  }, Configuration.MaxTurnLength);
  const botChannel = message.guild.channels.find('id', Configuration.BotPlaygroundChannelId);
  botChannel.send(channelMessage);
}

async function setNewHangmanWord() {
  const randomPageUrl = `${Configuration.WebsiteApiUrl}pages/random?private_token=${Keys.ApiPrivateToken}`
  let pageObject = await Axios.get(randomPageUrl);
  let getAttemptMax = 5, currentAttempt = 1;
  
  // Attempt several times to get a random word (in case it's not a valid game page or something)
  while (pageObject.data.response_code !== '200' && 
         currentAttempt < getAttemptMax) {
    pageObject = await Axios.get(`${Configuration.WebsiteApiUrl}pages/random?private_token=${Configuration.ApiPrivateToken}`);
    currentAttempt += 1;
  }
  
  // If we're still having errors, there's a problem
  if (pageObject.data.response_code !== '200') {
    currentAnswer = `ERROR - (code: ${pageObject.data.response_code}, error: ${pageObject.data.response_error})`;
    return;
  }
  
  // Set the answer to the information we want to gather
  // For now, just the title since it's easier
  currentAnswer = pageObject.data.pageTitle;
  
  // Save the URL
  webUrl = `https://www.legendoflegaia.net/games/${pageObject.data.gameId}/${pageObject.data.id}`;
  
  // Setup the underscores for the players
  for (let i = 0; i < currentAnswer.length; i++) {
    if (currentAnswer.charAt(i).match(/[a-zA-Z0-9]/i)) {
      currentWord += '_';
    } else if (currentAnswer.charAt(i) !== ' ') {
      currentWord += currentAnswer.charAt(i);
    } else {
      currentWord += ' ';
    }
  }
  
  // Log it just in case it's needed
  console.log(currentAnswer);
  console.log(webUrl);
}

function showHangman() {
  let ascii = '```\n' +
              '_________\n' +
              '|       |\n';
  let segments = 4; //the number of segments left to the gallows fromt he ground
  
  // determine if it's done, or if we need to draw the head
  if (incorrectAttempts > 0) {
    ascii += '|       O\n';
    segments--;
  }
  
  // Body and/or Arms
  if (incorrectAttempts === 2) {
    ascii += '|       |\n';
    segments--;
  } else if (incorrectAttempts === 3) {
    ascii += '|      /|\n';    
    segments--;
  } else if (incorrectAttempts >= 4) {
    ascii += '|      /|\\\n'; 
    segments--;
  }
  
  // Legs
  if (incorrectAttempts === 5) {
    ascii += '|      / \n';
    segments--;
  } else if (incorrectAttempts >= 6) {
    ascii += '|      / \\\n'; 
    segments--;
  }
  
  for (let i = 0; i < segments; i++) {
    ascii += '|\n';
  }
  ascii += `\nCurrent word: ${currentWord.split('').join(' ')}\n` +
           `\nCurrent guesses: ${badGuesses}\n` +
           '```';
  if (incorrectAttempts === maxAttempts || currentWord === currentAnswer) {
    ascii += `The correct answer is ${currentAnswer}!\n` +
             `To learn more about this, visit ${webUrl}`;
  }
  
  return ascii;
}