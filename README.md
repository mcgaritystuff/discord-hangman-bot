All games are handled through the game_manager. They must follow the schema
given in the game model. After a game is created, you must add it to the
`gamesAvaiable` object in `games/game_manager.js`. Lastly, it needs to be 
required into `game_manager.js` as well as contain the following exported 
async functions:

1. joinGame(message)
1. startGame(message, client)
1. play(message, gameMessage) // gameMessage is already stripped of the command `,game` for you :)


To install, you need to create two files in the config directory:

# keys.js

```js
module.exports = {
  "Token": "YOUR-DISCORD-TOKEN",
  "DatabaseUrl": "MONGO-DATABASE-URL",
  "ApiPrivateToken": "YOUR-SECRET-API-TOKEN"
};
```

# configuration.js

(Note the ending forward slash in WebsiteApiUrl)

```js
module.exports = {
  "BotOwnerId": "ownerId",
  "BotPlaygroundChannelId": 'channelId',
  "WebsiteApiUrl": "https://some.awesome.site/api/",
  "GameStartTime": 30, //milliseconds
  "MaxTurnLength": 30000, //milliseconds
  "GlobalNoResponseTimeout": 300000 //milliseconds
};
```