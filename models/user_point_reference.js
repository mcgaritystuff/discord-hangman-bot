const Mongoose = require('mongoose');
const { Schema } = Mongoose;

const userPointReferenceSchema = new Schema({
  "PlayerId": String,
  "GameName": String,
  "Points": Number
});

Mongoose.model('userPointReferences',userPointReferenceSchema);