const Mongoose = require('mongoose');
const { Schema } = Mongoose;

const gameSchema = new Schema({
  "GameName": String,
  "InProgress": Boolean,
  "WaitingForPlayers": Boolean
});

Mongoose.model('games', gameSchema);