const Mongoose = require('mongoose');
require('./models/user_point_reference');
require('./models/game');

const GameBot = require('./bot/gamebot').GameBot;
const Keys = require('./config/keys');
const Configuration = require('./config/configuration');

Mongoose.connect(Keys.DatabaseUrl);

const gamebot = new GameBot(Keys.Token);
const Client = gamebot.login();

Client.on('ready', async () => {
  gamebot.ready();
});

Client.on('message', msg => {
  gamebot.handleMessage(msg);
});
